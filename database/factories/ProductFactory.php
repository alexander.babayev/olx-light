<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $productNumber = Product::latest()->first()->id + 1;
        return [
            'user_id' => User::all()->random()->id,
            'name' => 'Product #'.$productNumber,
            'description' => 'This is a description for the product #'.$productNumber,
            'price' => rand(0,99999)/100
        ];
    }
}
