# OLX Light REST API Service

This is a test project assigned by MetaProject to Alexander Babayev with a purpose to test his abilities if he is able to produce desirable IT solutions as a PHP (Laravel) Software Developer.


## Requirements

- PHP 7.4.7+
- Laravel 8.52.0+
- PostgreSQL 12.7+
- composer 2.0.8+


## Installation (for developers)

To deploy the service on you local machine use your command line tool (cmd, bash, terminal, iTerm, etc.), cd into your web root directory and execute next set of commands:


### Get the source code and create your feature branch

```bash
git clone https://gitlab.com/alexander.babayev/olx-light.git
cd olx-light
git checkout -b mybranch develop
composer install
```


### Create and configure the '.env' file

Copy '.env.example' to '.env' file.
```bash
cp .env.example .env
```

Open the '.evn' file in your text editor and replace next parameters values with yours:

```text
APP_NAME=OlxLight
APP_URL=http://127.0.0.1:8000

DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=olxlight
DB_USERNAME=postgres
DB_PASSWORD=
```

Generate application key:

```bash
php artisan key:generate
```

### Create a database

Connect to you PostreSQL server with your favorite command line tool and create a new database:

```bash
create database olxlight;
```

Get back to you OS command line tool into the project's directory and execute:

```bash
php artisan migrate
```


## Usage

### Run the service

```bash
php artisan serve --port 8000
```

### Send API requests

Use an API development tool like [Postman](https://www.postman.com/) in order to send requests to the running service.

#### Available routines (endpoints, all prefixed with 'http://127.0.0.1:8000/api/')
```text
- post('register') // Register (create) a new user
- post('login')    // Login (user authentication)
- post('logout')   // Logout (authorization bearer token is required)

- get('products')  // List products
- post('products') // Create a new product (authorization bearer token is required)

- post('products/{product_id}/images')        // Create (upload) product image (authorization bearer token is required)
- delete('products/{product_id}/images/{id}') // Delete product image (authorization bearer token is required)

- get('products/{id}')    // Get product
- put('products/{id}')    // Update product (authorization bearer token is required)
- delete('products/{id}') // Delete product (authorization bearer token is required)
```
