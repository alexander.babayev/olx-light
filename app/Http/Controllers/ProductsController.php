<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Models\Product;
use App\Models\ProductImage;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        request()->validate([
            'sort' => 'string|in:price,-price,created_at,-created_at',
            'ipp' => 'integer|min:10|max:50',
            'page' => 'integer|min:1',
        ]);

        
        $sort = request()->input('sort', 'id');
        $order = 'asc';
        if ($sort[0] == '-') {
            $sort = ltrim($sort,'-');
            $order = 'desc';
        }

        
        $ipp = request()->input('ipp', 10);
        $products = Product::select(['id','name','price','created_at'])
            ->with('image')
            ->orderBy($sort, $order)
            ->simplePaginate($ipp);
        
        $products->each(function($product) {
            $product->name = Str::limit($product->name, 50, '...');
        });

        
        $params = [];
        if ($sort != 'id') $params['sort'] = ($order=='asc'?'':'-').$sort;
        $params['ipp'] = $ipp;
        $products->appends($params);
        
        
        return $products;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|max:255',
            'description' => 'required|max:1000',
            'price' => 'required|numeric|between:0,999999.99',
        ]);
        $data['user_id'] = auth()->user()->id;
        
        
        $product = Product::create($data);
        return response(['product' => $product], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::with('images')->find($id);
        if (is_null($product)) return response(['message' => 'Product not found'], 404);
        return response(['product' => $product], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        if (is_null($product) or $product->user_id != auth()->user()->id)
            return response(['message' => 'Product not found'], 404);

        
        $data = $request->validate([
            'name' => 'min:1|max:255',
            'description' => 'min:1|max:1000',
            'price' => 'numeric|between:0,999999.99',
        ]);
        
        
        $product->update($data);
        return response(['product' => $product], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::with('images')->find($id);
        if (is_null($product) or $product->user_id != auth()->user()->id)
            return response(['message' => 'Product not found'], 404);
        

        $product->images->each(function ($productImage) {
            $filepath = 'public/products/'.basename($productImage->link);
            if (Storage::exists($filepath)) Storage::delete($filepath);
        });
        Product::destroy($id);


        return response(['success' => true], 200);
    }
}
