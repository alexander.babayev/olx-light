<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Product;
use App\Models\ProductImage;

class ProductImagesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Integer  $productId
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $productId)
    {
        $product = Product::find($productId);
        if (is_null($product) or $product->user_id != auth()->user()->id)
            return response(['message' => 'Product not found'], 404);
        
        $request->validate([
            'image' => 'required|mimes:jpg,jpeg,png|max:4096'
        ]);

        
        $filename = uniqid().'.'.$request->image->extension();
        $link = $request->file('image')->storeAs('products', $filename, 'public');
        
        $productImage = ProductImage::create([
            'product_id' => $productId,
            'link' => env('APP_URL').'/storage/'.$link
        ]);


        return response(['image' => $productImage], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $productId
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($productId, $id)
    {
        $product = Product::find($productId);
        if (is_null($product) or $product->user_id != auth()->user()->id)
            return response(['message' => 'Product not found'], 404);

        $productImage = ProductImage::find($id);
        if (is_null($productImage) or $productImage->product_id != $productId)
            return response(['message' => 'Image not found'], 404);


        $filepath = 'public/products/'.basename($productImage->link);
        if (Storage::exists($filepath)) Storage::delete($filepath);
        ProductImage::destroy($id);
        
        
        return response(['success' => true], 200);
    }
}
