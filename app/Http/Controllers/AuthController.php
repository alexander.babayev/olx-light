<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|regex:/(.+)@(.+)\.(.{2,})/i|iunique:users,email|max:255',
            'name' => 'required|regex:/^[a-zа-яA-ZА-ЯЁё ]+$/u|min:1|max:255',
            'surname' => 'required|regex:/^[a-zа-яA-ZА-ЯЁё ]+$/u|min:1|max:255',
            'password' => ['required', 
                'min:8',
                'regex:/^.*(?=.{3,})(?=.*[A-Z])(?=.*[a-z])(?=.*[~!@#$%^&*]).*$/',
                'confirmed']
        ], [
            'email.iunique' => 'The email is already registered.',
            'name.regex' => 'The name must consist of letters only.',
            'surname.regex' => 'The surname must consist of letters only.',
            'password.regex' => 'The password must contain at least one lowercase letter, one uppercase latter and one specsymbol (~!@#$%^&).',
        ]);

        
        $user = User::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'email' => Str::lower($data['email']),
            'password' => bcrypt($data['password']),
        ]);

        
        return response(['user' => $user], 201);
    }


    public function login(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        
        $user = User::where('email', Str::lower($data['email']))->first();
        if (!$user || !Hash::check($data['password'], $user->password))
            return response(['message' => 'Invalid credentials'], 401);


        $token = $user->createToken('token')->plainTextToken;
        $response = [
            'user' => $user,
            'token' => $token
        ];
        return response($response, 201);
    }


    public function logout(Request $request)
    {
        auth()->user()->tokens()->delete();
        return ['message' => 'Logged out.'];
    }

}
